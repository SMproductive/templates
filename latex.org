#+LaTeX_CLASS: article
#+LATEX_COMPILER: xelatex
#+LATEX_CLASS_OPTIONS: [a4paper, 11pt, twoside]

#+LATEX_HEADER: \usepackage[left=3cm,right=2cm, top=2cm, bottom=2cm]{geometry}

# For section title formatting
#+LATEX_HEADER: \usepackage{titlesec}

# For text color
#+LATEX_HEADER: \usepackage{xcolor}

# For correct quotation format
#+LATEX_HEADER: \usepackage[autostyle=true]{csquotes}

# For additional math typesettings
#+LATEX_HEADER: \usepackage{amsmath}

# Set numbering based on section
#+LATEX_HEADER: \numberwithin{equation}{section}
#+LATEX_HEADER: \numberwithin{figure}{section}

# For font setting
#+LATEX_HEADER: \usepackage{fontspec}
#+LATEX_HEADER: \setmainfont{FreeSerif}

# For bibliography, have latexmk installed!
#+CITE_EXPORT: natbib
#+LATEX_HEADER: \usepackage[numbers]{natbib}

# For custom page numbering
#+LATEX_HEADER: \usepackage{fancyhdr}   % To customize headers and footers
#+LATEX_HEADER: \usepackage{lastpage}   % To get the total number of pages
#+LATEX_HEADER: \pagestyle{fancy}       % Use fancy page style
#+LATEX_HEADER: \fancyhf{}              % Clear the default header and footer
#+LATEX_HEADER: \fancyfoot[C]{Page \thepage\ of \pageref{LastPage}} % Set footer


# For formatting hyperlinks
# Hypersetup is set in doom configuration
